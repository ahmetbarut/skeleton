<?php

namespace Digitalcake\TodoListManagment\Providers;

use Illuminate\Events\EventServiceProvider;
use Illuminate\Support\ServiceProvider;

class TodoListManagmentEventServiceProvider extends EventServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        //
    ];

    public function register()
    {
        parent::register();
    }

    public function boot()
    {
        //
    }
}
