<?php

namespace Digitalcake\MeetingNotes\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class TodoListManagmentServiceProvider extends ServiceProvider
{

    protected string $packageName;

    public function __construct($app)
    {
        parent::__construct($app);
        $this->packageName = $this->packageName();

        if (!file_exists(($dir = __DIR__ . '/../../config/' . $this->packageName . '.php'))) {
            copy(__DIR__ . '/../../config/skeleton.php', $dir);
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../../config/' . $this->packageName . '.php',
            $this->packageName
        );

        $this->app->register(MeetingNotesEventServiceProvider::class);
    }

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerCommands();
        $this->scheduleTasks();
        $this->registerViews();
        $this->registerRoutes();
        $this->registerTranslations();
        $this->registerMigrations();
        $this->registerPaginator();

        $this->publishes([
            __DIR__ . '/../../resources/views' => base_path(
                'resources/views/vendor/' . $this->packageName
            ),
            __DIR__ . '/../../resources/lang' => base_path(
                'resources/lang/vendor/' . $this->packageName
            ),
            __DIR__ . '/../../resources/assets' => base_path(
                'resources/assets/vendor/' . $this->packageName
            ),
            __DIR__ . '/../../config/skeleton.php' => config_path(
                $this->packageName . '.php'
            ),
        ], $this->packageName);
    }

    protected function scheduleTasks()
    {
        /**
         * @var \Illuminate\Console\Scheduling\Schedule $artisan
         */
        $artisan = $this->app->make(\Illuminate\Console\Scheduling\Schedule::class);

        // $artisan->call(function () {

        // })->addMonths();
    }

    protected function registerCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                //
            ]);
        }
    }

    protected function registerRoutes()
    {
        $this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');
    }

    protected function registerViews()
    {
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', $this->packageName);
    }

    protected function registerTranslations()
    {
        $this->loadTranslationsFrom(__DIR__ . '/../../resources/lang', $this->packageName);
    }

    protected function registerMigrations()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');
    }

    protected function registerPaginator()
    {
        Paginator::defaultView('pagination::' . config($this->packageName . '.paginator-theme'));
        Paginator::defaultSimpleView('pagination::simple-' . config($this->packageName . '.paginator-theme'));
    }

    protected function packageName(): string
    {
        $config = require __DIR__ . '/../../config/skeleton.php';
        $str = Str::of($config['package-name']);

        if ($str->isEmpty()) {
            throw new \Exception('package-name config parameter is not set.');
        }

        if (!$str->startsWith('dc-')) {
            $str = $str->prepend('dc-');
        }

        return $str->kebab(config('skeleton.package-name'));
    }
}
