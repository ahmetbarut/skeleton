<?php

return [
    /**
     * Paket'i yayınlarken bu isimden yayınlayacaktır. Yapılandırma, view isimleri, vb.
     */
    'package-name' => null,
    
    /**
     * -------------------------------------------------------------------------
     * |---------            Kullanılabilir temalar.              -------------|
     * -------------------------------------------------------------------------
     * default
     * tailwind
     * bootstrap-4
     *
     */
    'paginator-theme' => 'bootstrap-4',
];
