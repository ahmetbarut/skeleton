# Paket İskeleti

Bir paket oluşturmadan önce bu repoyu kullanarak hazır iskelet sağlar.

- [Paket İskeleti](#paket-iskeleti)
- [Yapılandırma](#yapılandırma)
  - [Config](#config)
  - [Composer](#composer)
  - [Service Provider](#service-provider)
  - [Commands](#commands)
  - [Events](#events)
  - [Schedule](#schedule)

## Yapılandırma

### config
`config/skeleton.php` dosyasından paket adını ve pakette kullanılacak bütün yapılandırmaları tanımlamanız gereklidir. Ekstradan bir `config` dosyası eklemenize gerek yok.

> `config/skeleton.php` dosyasının adını kesinlikle değiştirmeyin !
> Paket isimleri `kebab-case` şeklinde olmalıdır. Eğer siz `camelCase` şeklinde isim verirseniz, yine `kebab-case` olarak çevirecektir. 
> Eğer `config` dosyası içinden bir ayar çekmek istiyorsanız, `config/skeleton.php` dosyasında `package-name` alanında belirttiğiniz isimde erişim sağlanabilir.


Örn:

```php
  // config/skeleton.php

  return [
    'package-name' => 'TestPackage',
    'emails' => [
      'admin' => 'admin@digitalcake.com.tr',
    ]
  ];

  // SkeletonController.php@index
  
  public function index()
  {
      config('test-package.emails.admin');
  }
```

### Composer
`composer.json` dosyasında da `psr-4` ve `name` kısımlarında belirtmeniz gereklidir. 

```json
{
    "name": "digitalcake/test-package",
    "type": "package",
    "autoload": {
        "psr-4": {
            "Digitalcake\\TestPackage\\": "src/"
        }
    }
}
```

### Service Provider
Service Provider dosyalarının isimlerini paket adına göre `psr-4`'e  uygun şekilde isim veriniz.

Örn:
```txt
SkeletonServiceProvider.php => TestPackageServiceProvider.php
SkeletonEventServiceProvider.php => TestPackageEventServiceProvider.php
```

`composer.json` dosyasına eklenmesi gerekiyor.

```json
  ...
   "extra": {
        "laravel": {
            "providers": [
                "Digitalcake\\TestPackage\\Providers\\TestPackageServiceProvider",
                "Digitalcake\\TestPackage\\Providers\\TestPackageEventServiceProvider"
            ]
        }
    }
    ...
```

### Commands
Komut dosyalarının isimlerini paket adına göre `psr-4`'e  uygun şekilde isim veriniz.

Komutu `TestPackageServiceProvider.php` dosyasında `registerCommands` metoduna ekleyiniz. 

```php
  public function registerCommands()
  {
      $this->commands([
          'TestPackage\Commands\TestCommand',
      ]);
  }
```

### Events
Event dosyalarının isimlerini paket adına göre `psr-4`'e  uygun şekilde isim veriniz.

Eğer `event` ve `listener` olacaksa `TestPackageEventServiceProvider.php` dosyasında `$listen` değişkenine ekleyiniz. 

```php
  protected $listen = [
      'TestPackage\Events\TestEvent' => [
          'TestPackage\Listeners\TestListener',
      ],
  ];
```

### Schedule
Eğer `schedule` olacaksa `TestPackageEventServiceProvider.php` dosyasında `scheduleTasks` metoduna ekleyiniz. 

```php
    protected function scheduleTasks()
    {
        /**
         * @var \Illuminate\Console\Scheduling\Schedule $artisan
         */
        $artisan = $this->app->make(\Illuminate\Console\Scheduling\Schedule::class);

        $artisan->command('model:prune')->daily();
    }
```